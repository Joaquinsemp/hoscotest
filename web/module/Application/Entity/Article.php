<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\String_;

/**
 * Application\Entity\Article
 *
 * @ORM\Table(name="article")
 * @ORM\Entity
 */
class Article
{
    /* other annotations */

    /**
     * @var integer $publisher
     *
     * @ORM\ManyToOne(targetEntity="Publisher", fetch="EAGER")
     * @ORM\JoinColumn(name="publisher", referencedColumnName="id")
     */
    private $publisher;

    public $text;


    /* other annotations and methods */

    // Returns Publisher of this post.
    public function getPublisher()
    {
        return $this->publisher;
    }

    // Sets Publisher of this post.
    public function setPublisher($publisher)
    {
        $this->publisher = $publisher;
    }

    // Returns text of this post.
    public function getText()
    {
        return $this->text;
    }

    // Sets Text of this post.
    public function setText($text)
    {
        $this->text = $text;
    }
}